# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "1"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "sa"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = "Berlin123!"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAlrUQfcgFlcOhChxKYMSRjw7IXiMpObIgTuQlacxRb+l7JBj3jXbh5s+MKlsn/Ed8Uh/1C18OfJ1I9Wcqskzre+jRwB+PoUUhQpSvMgQ30jUPNn3UZDFe0RUKndtk9+RY/MfnbDPPKmPt8g+O39oRPU2LVk4bGMFG+noZ9NLsgwiLRd0jZIzK+IY1BOhLuxG4WXkr5mHqkZ/UISuvxmKs0J3vaV1S6lA/Dh6LAPbBdUL+z7swHuZCAorUmEUbfI2Zv8TrzVxpbTIBm3TcsZ5rdWXmgLYtVQSywlxzDYpYP8K0LtBjnd0p9/Vao5U8Zeigla0F0CGQzWKqGZkVRUtSfQ== rsa-key-20181011"
}
