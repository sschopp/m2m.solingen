write_files:
  - encoding: b64
    path: /home/ubuntu/run.sh
    content: "${run_script}"
    permissions: '0644'

runcmd:
  - mkdir -p /home/ubuntu/app
  - aws s3 cp "s3://${router_service_bucket}/${router_service_tarball}" /home/ubuntu/app/
  - cd /home/ubuntu/app && tar xvzf ${router_service_tarball}
  - cd /home/ubuntu/app && ./run.sh
