var express = require('express')
var proxy = require('express-http-proxy')
const url = require('url')
var app = express()

try {
    var app = express()

    app.use('/modlog', proxy(process.env.MODLOG_DNS_NAME))
    app.use('/*', proxy(process.env.LOBSTERS_DNS_NAME, {
        forwardPath: req => url.parse(req.baseUrl).path,
        preserveHostHdr: true
    }))
        
} catch (error) {
    console.log('test')
    console.log(error)
}

app.listen(1972, ()  => console.log(' listening on port 1972'))